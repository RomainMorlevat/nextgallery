This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

-   Add NEXTAUTH_PASSWORD and NEXTAUTH_USERNAME to your .env.local to handle login.

-   Generate API file by running `node api_generator.js` from `api_generator/` directory.
It's formatted this way:
```json
[
  {
    "description": "This is a description",
    "height": 600,
    "fileName": "picture_name.jpeg",
    "keywords": ["keyword"],
    "title": "This is a title",
    "width": 800
  }
]
```

-   Add API_DIRECTORY key to .env.local with an url pointing to a directory where you store the API json file generated. 

Run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/import?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
