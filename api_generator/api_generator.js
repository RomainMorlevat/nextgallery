const fs = require('fs')
const iptc = require('node-iptc')
const path = require('path')
const sizeOf = require('image-size')

const imagesFolder = './'

const pictures = fs.readdirSync(imagesFolder).filter(file => {
  return path
    .extname(file)
    .toLowerCase()
    .match(/.jpg|.jpeg/i)
})
const result = pictures.map(file => {
  const image = fs.readFileSync(file)
  const { date_created: dateCreated, keywords, caption } = iptc(image)
  const { height, width } = sizeOf(image)

  const cleanedCaption = typeof caption === 'undefined' ? '' : caption.trim()

  return {
    date: dateCreated,
    fileName: file,
    height: height,
    keywords: keywords,
    title: cleanedCaption,
    width: width
  }
})

fs.writeFileSync('./pictures.json', JSON.stringify(result), 'utf8')
