import Image from 'next/image'

const Modal = ({ handleClose, isOpen = false, picture }) => {
  const apiPath = 'https://www.romainmorlevat.com/h0m3/'
  const windowHeight = window.innerHeight

  if (!isOpen) return null

  return (
    <div className='fixed z-10 inset-0 overflow-y-auto' onClick={handleClose}>
      <div className='flex items-end justify-center min-h-screen pt-2 px-2 pb-2 text-center sm:block sm:p-0'>
        <div className='fixed inset-0 transition-opacity' aria-hidden='true'>
          <div className='absolute inset-0 bg-gray-500 opacity-75' />
        </div>

        <span
          className='hidden sm:inline-block sm:align-middle sm:h-screen'
          aria-hidden='true'
        >
          &#8203;
        </span>

        <div
          className='align-bottom bg-white overflow-hidden rounded-sm text-left
            inline-block shadow-xl sm:align-middle sm:max-w-7xl
            sm:my-4 sm:w-full transform transition-all'
          role='dialog'
          aria-modal='true'
          aria-labelledby='modal-headline'
        >
          <div className='bg-black content-center flex max-h-screen p-1 pb-0'>
            <div className='m-auto'>
              <Image
                alt={picture.title}
                height={windowHeight}
                src={apiPath + picture.fileName}
                title={picture.title}
                width={picture.width * windowHeight / picture.height}
              />
            </div>
          </div>
          <div className='bg-black p-1 pt-0 flex'>
            <p className='flex-grow text-gray-100'>{picture.title}</p>
            <button
              onClick={handleClose}
              type='button'
              className='h-5 w-5 inline-flex justify-center rounded-full border
                border-gray-300 shadow-sm bg-white text-base font-medium
                text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2
                focus:ring-indigo-500'
            >
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth={2}
                  d='M6 18L18 6M6 6l12 12'
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Modal
