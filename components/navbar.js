import { signIn, signOut, useSession } from 'next-auth/client'
import Link from 'next/link'

const Navbar = () => {
  const [session] = useSession()

  return (
    <nav className='bg-gray-800'>
      <div className='max-w-7xl mx-auto px-2 sm:px-6 lg:px-8'>
        <div className='relative flex items-center justify-between h-16'>
          <div className='flex-1 flex items-center justify-center sm:items-stretch sm:justify-start'>
            <div className='flex-shrink-0 flex items-center text-white'>
              JajaRoro
            </div>
            <div className='hidden sm:block sm:ml-6'>
              <div className='flex space-x-4'>
                {session &&
                  <Link href='/'>
                    <a
                      href='#'
                      className='bg-gray-900 text-white px-3 py-2 rounded-md text-sm font-medium'
                    >
                      Photos
                    </a>
                  </Link>}
              </div>
            </div>
          </div>
          <div className='absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0'>
            {session && (
              <button
                className='bg-gray-800 p-1 rounded-md text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white'
                onClick={signOut}
              >
                Sign out
              </button>
            )}
            {!session && (
              <button
                className='bg-gray-800 p-1 rounded-md text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white'
                onClick={signIn}
              >
                Sign in
              </button>
            )}
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
