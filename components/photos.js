import Image from 'next/image'
import { useEffect, useState } from 'react'
import fetch from 'unfetch'

import Modal from './modal'

const Photos = () => {
  const apiPath = 'https://www.romainmorlevat.com/h0m3/'
  const [currentPicture, setCurrentPicture] = useState(null)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [pictures, setPictures] = useState([])
  const [sortAsc, setSortAsc] = useState(false)

  const handleModal = picture => {
    setCurrentPicture(picture)
    setIsModalOpen(!isModalOpen)
  }

  useEffect(() => {
    fetch('api/pictures')
      .then(result => result.json())
      .then(data => setPictures(data.reverse()))
  }, [])

  const sortByDate = (a, b) => {
    if (a.date > b.date) {
      return sortAsc ? -1 : 1
    } else if (a.date < b.date) {
      return sortAsc ? 1 : -1
    }
    return 0
  }

  const handleSortByDate = () => {
    const sortedPictures = pictures.slice(0).sort(sortByDate)
    setSortAsc(!sortAsc)
    setPictures(sortedPictures)
  }

  return (
    <>
      <main className='container m-auto px-1 pt-1'>
        <Modal
          handleClose={() => setIsModalOpen(false)}
          isOpen={isModalOpen}
          picture={currentPicture}
        />

        <div className='mb-2 text-right'>
          <button
            className='bg-white focus:outline-none focus:ring-2 focus:ring-indigo-500
              focus:ring-offset-2 font-medium group hover:text-gray-900 inline-flex
              items-center text-base text-gray-500'
            onClick={handleSortByDate}
          >
            <span>Trier par date</span>

            <svg
              aria-hidden='true'
              className={`text-gray-400 ml-2 h-5 w-5 group-hover:text-gray-500 transform transition ${sortAsc ? 'rotate-180' : ''}`}
              fill='currentColor'
              viewBox='0 0 20 20'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                clipRule='evenodd'
                d='M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z'
                fillRule='evenodd'
              />
            </svg>
          </button>
        </div>

        <div className='flex flex-wrap justify-between content-center'>
          {pictures.map((picture, index) => (
            <div key={index}>
              <button onClick={() => handleModal(picture)}>
                <Image
                  alt={picture.title}
                  height={picture.height / 4 || 200}
                  key={picture.fileName}
                  src={apiPath + picture.fileName}
                  title={picture.title}
                  width={picture.width / 4 || 300}
                />
              </button>
            </div>
          ))}
        </div>
      </main>
    </>
  )
}

export default Photos
