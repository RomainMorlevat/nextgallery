import { Provider } from 'next-auth/client'
import '../styles/tailwind.css'
import Navbar from '../components/navbar'

function MyApp ({ Component, pageProps }) {
  return (
    <Provider session={pageProps.session}>
      <Navbar />
      <Component {...pageProps} />
    </Provider>
  )
}

export default MyApp
