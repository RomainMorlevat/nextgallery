import fetch from 'node-fetch'

export default async function handler (req, res) {
  const response = await fetch(process.env.API_DIRECTORY + 'pictures.json')
  const pictures = await response.json()
  res.status(200).json(pictures)
}
