import Head from 'next/head'
import { useSession } from 'next-auth/client'
import Photos from '../components/photos'

export default function Home () {
  const [session, loading] = useSession()

  if (loading) {
    return <p>Loading...</p>
  }

  return (
    <>
      <Head>
        <title>Jaja et Roro</title>
        <link rel='icon' href='/heart.svg' />
      </Head>

      <main className='container m-auto px-1 pt-1'>
        {session && <Photos />}
      </main>
    </>
  )
}
